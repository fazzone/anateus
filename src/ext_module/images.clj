(ns ext-module.images
  (:require [clojure.java.io :as io]
            [ext-module.model :as model])
  (:import [javax.imageio ImageIO]
           [java.awt.image BufferedImage]
           [java.io File]
           [java.awt Color Graphics2D]))

(def ^:dynamic *textures-path* "/home/russell/.local/share/Steam/steamapps/common/Opus Magnum/Content/textures" )

(defn apply-lightramp
  [^BufferedImage ramp ^BufferedImage mask]
  (let [val->color (into {}
                         (for [i (range (.getWidth ramp))]
                           [i (.getRGB ramp i 0)]))]
    (dotimes [x (.getWidth mask)]
      (dotimes [y (.getHeight mask)]
        (let [mask-rgb (.getRGB mask x y)
              mask-shade (bit-and mask-rgb 0xff)]
          (.setRGB mask x y (get val->color mask-shade)))))
    mask))

(defn tex
  ^File [& parts]
  (if (= parts ["atoms" "quicksilver_diffuse.png"])
    (recur ["atoms" "salt_diffuse.png"])
    (apply io/file *textures-path* parts)))

(defn whiteout
  [^BufferedImage whiteout ^BufferedImage image]
  (dotimes [x (.getWidth image)]
    (dotimes [y (.getHeight image)]
      (when (not= -1 (.getRGB whiteout x y))
        (.setRGB image x y 0))))
  image)

(defn combine-images
  [images]
  (let [^BufferedImage proto-img (first images)
        acc-image (BufferedImage. (.getWidth proto-img) (.getHeight proto-img) BufferedImage/TYPE_INT_ARGB)
        ^Graphics2D g (.createGraphics acc-image)]
    (doseq [i images]
      (.drawImage g i 0 0 nil))
    acc-image))

(defn get-atom-image
  [a]
  (let [ramp (tex "atoms" (str (name a) "_lightramp.png"))
        images (for [p [:diffuse :symbol]
                     :let [^File f (tex "atoms" (str (name a) "_" (name p) ".png"))]
                     :when (.exists f)]
                 (ImageIO/read f))]
    (combine-images
     (cond->> images
       (.exists ramp)
       (cons (apply-lightramp (ImageIO/read ramp)
                              (ImageIO/read (tex "atoms" "atom.lighting" "top.png"))))))))

(defn get-element-image
  [e]
  (combine-images
   (for [p [:base :shadow :colors :symbol]
         :let [^File f (tex "atoms" "elements" (str (name e) "_" (name p) ".png"))]
         :when (.exists f)]
     (ImageIO/read f))))

(def type->image
  (delay
   (let [whiteout-mask (ImageIO/read (tex "atoms" "whiteout.png"))]
     (into
      {}
      (concat
       (for [at model/atom-types]
         [at (whiteout whiteout-mask (get-atom-image at))])
       (for [et model/element-types]
         [et (whiteout whiteout-mask (get-element-image et))]))))))




