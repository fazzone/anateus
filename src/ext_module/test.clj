(ns ext-module.test
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [ext-module.read-solution :as read-solution]
            [ext-module.render :as render]
            [ext-module.hex :as hex]
            [ext-module.render.atoms :as render-atoms]
            [ext-module.render.hex :as render-hex]
            [ext-module.render.parts :as render-parts]
            [ext-module.svg :as s])
  (:import [javax.imageio ImageIO]
           [java.awt.image BufferedImage]
           [java.awt Color BasicStroke]
           [java.awt.geom Point2D]
           [java.io File]
           [java.awt Color Graphics2D]))

(def solution-file (io/file "solutions/seal-solvent-1.solution"))

(defn add-point
  [[x1 y1] [x2 y2]]
  [(+ x1 x2) (+ y1 y2)])

(defn draw-track
  [^Graphics2D g hexsize {:keys [position extra]}]
  (let [trackpoints (mapv #(add-point position %) extra)]
    (dotimes [idx (count trackpoints)]
      (let [[i j] (nth trackpoints idx)]
        (render/draw-hex g hexsize i j "")))
    (let [stroke (.getStroke g)]
      (.setStroke g (BasicStroke. 4))
      (dotimes [idx (dec (count trackpoints))]
        (let [[i1 j1] (nth trackpoints idx)
              [i2 j2] (nth trackpoints (inc idx))
              [x1 y1] (apply render/oddr-to-screen hexsize (render/axial-to-oddr i1 j1))
              [x2 y2] (apply render/oddr-to-screen hexsize (render/axial-to-oddr i2 j2))]
          (.drawLine g x1 y1 x2 y2)))
      (.setStroke g stroke))))

(defn draw-circle
  [^Graphics2D g x y r]
  (.drawOval g (- x r) (- y r) (* 2 r) (* 2 r)))

(defn fill-circle
  [^Graphics2D g x y r]
  (.fillOval g (- x r) (- y r) (* 2 r) (* 2 r)))

(defn draw-arm
  [^Graphics2D g hexsize i j size rotation]
  (let [prev-color (.getColor g)
        prev-stroke (.getStroke g)
        prev-tx (.getTransform g)
        arm-color Color/gray
        gripper-color (.darker Color/gray)
        center-color (.brighter Color/gray)
        gripper-radius (* 2 (/ hexsize 3))
        [x y] (apply render/oddr-to-screen hexsize (render/axial-to-oddr i j))
        hexwidth (render/size->width hexsize)
        angle (* rotation (/ Math/PI -3.0))]
    (.translate g (double x) (double y))
    (.rotate g angle)

    (.setColor g arm-color)
    (fill-circle g 0 0 (/ hexwidth 4))

    (.setStroke g (BasicStroke. 4))
    (let [armwidth (/ hexwidth 12)]
      (.fillRect g 0 (- 0 (/ armwidth 2)) (- (* size hexwidth) gripper-radius) armwidth))

    (.setStroke g (BasicStroke. 8))
    (.setColor g gripper-color)
    (draw-circle g (* size hexwidth) 0 gripper-radius)

    (.setColor g center-color)
    (fill-circle g 0 0 (/ hexwidth 8))

    (.setColor g prev-color)
    (.setStroke g prev-stroke)
    (.setTransform g prev-tx)))

(defn draw-hexarm
  [^Graphics2D g hexsize i j size]
  (dotimes [rot 6]
    (draw-arm g hexsize i j size rot)))

(def ^:const name->simple
  {"bonder-speed" "3bond"
   "bonder-prisma" "prisma"
   "glyph-projection" "proj"
   "glyph-calcification" "calc"
   "glyph-duplication" "dupe"})

(defn draw-object
  [^Graphics2D g hexsize {:keys [name position] :as obj}]
  (let [[x y] position]
    (render/draw-hex g hexsize x y (get name->simple name name))
    (case name
      "track" (draw-track g hexsize obj)
      "arm1" (draw-arm g hexsize x y (:size obj) (:rotation obj))
      "arm6" (draw-hexarm g hexsize x y (:size obj))
      "piston" (draw-arm g hexsize x y (:size obj) (:rotation obj))
      nil)))

(defn draw-prime
  [^Graphics2D g hexsize {:keys [prime position]}]
  (let [[i j] position
        [x y] (render/axial-to-screen i j)
        stroke (.getStroke g )]
    (.setStroke g (BasicStroke. 5))
    (draw-circle g x y (* 2 (/ hexsize 3)))
    (.setStroke g stroke)))

(defn write-solution-img
  [{:keys [objects] :as solution}]
  (let [[width-px height-px] [3840 2160]
        [center-x center-y] [(/ width-px 2) (/ height-px 2)]
        image (BufferedImage. width-px height-px BufferedImage/TYPE_INT_ARGB)
        ^Graphics2D g (.createGraphics image)
        hexsize 60]
    (.setColor g Color/white)
    (.fillRect g 0 0 width-px height-px)
    
    #_(.setStroke g (BasicStroke. 4))
    (.setColor g Color/black)
    (.translate g (double center-x) (double center-y))
    (render/draw-hex g hexsize 0 0 "0,0")

    
    (doseq [obj objects]
      (draw-object g hexsize obj))
    
    (ImageIO/write image "png" (io/file (str "/tmp/" (:puzzle solution) ".png")))))

(comment
  (time
   (->> (file-seq (io/file "solutions"))
        (filter #(and (.endsWith (str %) ".solution")))
        (pmap
         (fn [f]
           (write-solution-img (read-solution/process-file f))))
        (dorun))))


(set! *warn-on-reflection* true)

(spit
 "/tmp/saturn.svg"
 (s/svg
  [:g {:fill "none" :stroke "black" :stroke-width "5"
       :transform "translate(100,100)"}
   [:path {:stroke-miterlimit 4
           :d "M 5.892849999999953,0.0 L 5.892849999999953,29.000000000000057"}]
   [:path {:d "M 0.0,4.75 L 13.0,4.75"}]
   [:path {:stroke-miterlimit 4
           :d "M 19.892849999999953,35.00000000000006 C 18.892849999999953,36.00000000000006 17.892849999999953,37.00000000000006 16.892849999999953,37.00000000000006 C 15.892849999999953,37.00000000000006 13.892849999999953,36.00000000000006 13.892849999999953,34.00000000000006 C 13.892849999999953,32.00000000000006 14.892849999999953,30.000000000000057 16.892849999999953,28.000000000000057 C 18.892849999999953,26.0 20.892849999999953,22.0 20.892849999999953,18.0 C 20.892849999999953,14.0 18.892849999999953,10.0 14.892849999999953,10.0 C 11.109659999999963,10.0 7.892849999999953,12.0 5.892849999999953,16.0"}]
   [:ellipse {:cx 12.1 :cy 18.2 :rx 3 :ry 3 :stroke "none" :fill "red"}]
   [:ellipse {:cx 12.1 :cy 18.2 :rx 30 :ry 30 :stroke "red" }]]))

(comment
  (time
   (spit
    "/tmp/out.svg"
    (s/svg
     {:width 1000 :height 1000}
     (s/translate 500 500
                  [:g
                   (render-hex/hex-board 7 7)
                   (render-hex/at-hex 0 0 (render-atoms/atom-group :lead))
                   (render-hex/at-hex 1 0 (render-atoms/atom-group :fire))
                   (render-hex/at-hex 2 0 (render-atoms/atom-group :air))
                   (render-hex/at-hex 3 0 (render-atoms/atom-group :earth))
                   (render-hex/at-hex 0 1 (render-atoms/atom-group :salt))
                   (render-hex/at-hex 0 2 (render-atoms/atom-group :lead))
                   (render-hex/at-hex -1 1 (render-parts/arm-group 1 1))
                   (render-hex/at-hex -1 1 (render-parts/arm-group 1 2))])))))




(defn render-molecule
  [{:keys [primes bonds]} rotation]
  (into [:g]
        (concat
         (for [{:keys [source dest]} bonds]
           (render-atoms/bond-group (first source) (second source)
                                    (first dest) (second dest)))
         (for [{:keys [prime position]} primes]
           (render-hex/at-hex (first position)
                              (second position)
                              (render-parts/with-rotation
                                (- 0 rotation)
                                (render-atoms/atom-group prime)))))))

(defn render-object
  [index->reagent index->product {:keys [name position rotation] :as obj}]
  (let [[x y] position]
    (render-hex/at-hex
     x y
     (render-parts/with-rotation
       rotation
       (case name
         "input" (-> obj :index index->reagent (render-molecule rotation))
         "out-std" [:g {:opacity "0.3"}
                    (-> obj :index index->product (render-molecule rotation))]
         "arm1" (render-parts/arm-group (:size obj))
         "bonder" (render-parts/double-bonder-group))))))

(def test-molecule
  {:primes
   [{:prime :salt, :position [0 1]}
    {:prime :salt, :position [0 -1]}
    {:prime :lead, :position [0 0]}],
   :bonds
   [{:bond-type :normal, :source [0 -1], :dest [0 0]}
    {:bond-type :normal, :source [0 0], :dest [0 1]}]}
  #_{:primes
     [{:prime :salt, :position [-1 1]}
      {:prime :salt, :position [0 1]}
      {:prime :salt, :position [1 0]}
      {:prime :salt, :position [1 -1]}
      {:prime :salt, :position [0 -1]}
      {:prime :salt, :position [-1 0]}
      {:prime :lead, :position [0 0]}],
     :bonds
     [{:bond-type :normal, :source [-1 0], :dest [-1 1]}
      {:bond-type :normal, :source [-1 1], :dest [0 1]}
      {:bond-type :normal, :source [0 1], :dest [1 0]}
      {:bond-type :normal, :source [1 -1], :dest [1 0]}
      {:bond-type :normal, :source [0 -1], :dest [1 -1]}
      {:bond-type :normal, :source [-1 0], :dest [0 -1]}
      {:bond-type :normal, :source [0 -1], :dest [0 0]}
      {:bond-type :normal, :source [0 0], :dest [1 -1]}
      {:bond-type :normal, :source [0 0], :dest [1 0]}
      {:bond-type :normal, :source [0 0], :dest [0 1]}
      {:bond-type :normal, :source [-1 1], :dest [0 0]}
      {:bond-type :normal, :source [-1 0], :dest [0 0]}]})

(defn grip-offset
  [{:keys [primes]} atom grip]
  (->> primes
       (map :position)
       (some
        (fn [[x y]]
          (and
           (= (+ (:x atom) x) (:x grip))
           (= (+ (:y atom) y) (:y grip))
           (hex/->Point x y))))))

(defn arm-rotate-molecule
  [{:keys [position size rotation]} structure mpos]
  (let [pos (hex/->Point (first position) (second position))
        offset (grip-offset structure mpos (hex/project pos rotation size))
        proj (hex/project pos (hex/ccw rotation) size)]
    proj
    (println 'proj= proj)
    (println 'offset= offset)
    #_(hex/add proj offset)
    (hex/subtract proj offset)))

(defn write-frame
  [rot]
  (spit
    (format "/tmp/outs/out-%04d.svg" rot)
    (s/svg
      {:width 2000 :height 2000}
      (s/translate
        1000 1000
        (let [armx 0 army 1
              armsize 3
              rotA 2
              rotB 1]
          [:g
           (render-hex/hex-board 10 10)
           (render-hex/at-hex
             armx army
             (s/rotate
               rot
               #_[:animateTransform {"attributeType" "XML"
                                     "attributeName" "transform"
                                     "type" "rotate"
                                     "from" "0 0 0"
                                     "to" "360 0 0"
                                     "dur" "10s"
                                     "repeatCount" "indefinite"}]
      
               (let [proj (hex/project (hex/->Point 0 1) 0 armsize)]
                 
                 [:g
                  (render-parts/arm-group 3)
                  #_(render-hex/at-hex (:x proj) (:y proj) (render-molecule test-molecule 0))
                  (render-hex/at-hex (:x proj) (:y proj) (render-molecule test-molecule 0))])))

           #_(let [{:keys [x y]} (arm-rotate-molecule
                                   {:position [armx army]
                                    :rotation 2
                                    :size 3}
                                   test-molecule
                                   (hex/->Point mx my))]
               (->> (render-molecule test-molecule 0)
                 (render-hex/at-hex x y)))])))))

#_(time
  (dorun (pmap write-frame (range 360))))
 
(defn render-svg
  ([puzzle solution]
   (render-svg puzzle solution 0 0))
  ([puzzle solution offset-x offset-y]
   (let [index->reagent (into {} (map-indexed vector (:reagents puzzle)))
         index->product (into {} (map-indexed vector (:products puzzle)))]
     (s/svg
       {:width 1000 :height 1000}
       (s/translate
         500 500
         (render-hex/at-hex
           offset-x offset-y
           [:g
            (render-hex/hex-board 10 10)
            (map #(render-object index->reagent index->product %)
              (concat
                (filter (comp #{"input" "out-std" "bonder"} :name) (:objects solution))
                (reverse (filter (comp not #{"input" "out-std" "bonder"} :name) (:objects solution)))))]))))))

#_(time
 (let [puzf "puzzles/waterproof-sealant/waterproof-sealant.puzzle"
       #_ #_solf "puzzles/waterproof-sealant/zachs-own.solution"
       solf "/home/russell/.local/share/Opus Magnum/76561198048020892/waterproof-sealant-3.solution" 
       sol (read-solution/process-file solf)]
   (spit "/tmp/waterproof-sealant.svg"
         (render-svg
          (read-solution/process-puzzle puzf)
          sol
          -3 0))))


#_(let [#_ #_puzf "/home/russell/.local/share/Opus Magnum/76561198048020892/workshop/w1346396617.puzzle"
      puzf "/home/russell/.local/share/Opus Magnum/76561198048020892/workshop/w1289665637.puzzle"
      puzzle (read-solution/process-puzzle puzf)
      molecule (-> puzzle :products first)]
  (spit
   "/tmp/out.svg"
   (s/svg
    {:width 1000 :height 1000}
    #_(s/translate 500 500 [:g (render-atoms/atom-group :air)])
    #_(s/translate 500 500
                   [:g
                    (render-hex/hex-board 7 7)
                    (concat
                     (for [{:keys [source dest]} (:bonds molecule)]
                       (render-atoms/bond-group (first source) (second source)
                                                (first dest) (second dest)))
                     (for [{:keys [prime position]} (:primes molecule)]
                       (render-hex/at-hex (first position)
                                          (second position)
                                          (render-atoms/atom-group prime))))])))
  )

#_(spit
 "/tmp/out.svg"
 (s/svg
  {:width 1000 :height 1000}
  (s/translate 500 500 [:g
                        (render-hex/hex-board 3 3)
                        (render-hex/at-hex 0 0 (render-atoms/atom-group :water))
                        (for [i (range 6)]
                          [:g
                           (let [{:keys [x y]} (hex/project (hex/->Point 0 0) i 3)]
                             (render-hex/at-hex x y (render-atoms/atom-group :lead)))
                           (let [{:keys [x y]} (hex/project (hex/->Point 0 0) i 2)]
                             (render-hex/at-hex x y (render-atoms/atom-group :air)))
                           (let [{:keys [x y]} (hex/project (hex/->Point 0 0) i 1)]
                             (render-hex/at-hex x y (render-atoms/atom-group :salt)))])
                        
                        #_(render-hex/at-hex 1 0 (render-atoms/atom-group :lead))
                        #_(render-hex/at-hex -1 0 (render-atoms/atom-group :lead))])))
