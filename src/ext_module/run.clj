(ns ext-module.run
  (:require [ext-module.read-solution :as read-solution]
            [ext-module.hex :as hex]))





(time
 (let [puzf "puzzles/waterproof-sealant/waterproof-sealant.puzzle"
       #_ #_solf "puzzles/waterproof-sealant/zachs-own.solution"
       solf "/home/russell/.local/share/Opus Magnum/76561198048020892/waterproof-sealant-3.solution" 
       sol (read-solution/process-file solf)]
   (read-solution/process-puzzle puzf)
   sol))

