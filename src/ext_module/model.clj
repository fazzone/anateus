(ns ext-module.model)

(def ^:const element-types #{:air :earth :fire :water :quintessence})
(def ^:const atom-types #{:copper :gold :iron :lead :mors :vitae :quicksilver :silver :tin :salt :repetition})
