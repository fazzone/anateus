(ns ext-module.core
  (:require [clojure.java.io :as io])
  (:import [java.nio ByteBuffer ByteOrder]
           [java.nio.file Files]
           [java.io File]))

(set! *warn-on-reflection* true)

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(defn read-to-buffer
  ^ByteBuffer [path]
  (-> path (io/file) (.toPath) (Files/readAllBytes) (ByteBuffer/wrap)))

(defn read-buffer
  [^ByteBuffer buf]
  (prn (vec (repeatedly 4 #(.get buf))))
  )

(defn read-str
  [^ByteBuffer b]
  (let [len (.get b)]
    (apply str (repeatedly len #(char (.get b))))))

(defn expect
  ([exp val info]
   (when (not= val exp)
     (printf "warning - expected %s but got %s (%s) \n" exp val info))))

(defn read-solution-info
  [^ByteBuffer buf]
  (let [n (.getInt buf)]
    (if (zero? n)
      {:solved false}
      (let [unkA (.getInt buf)
            cycles (.getInt buf)
            unkB (.getInt buf)
            cost (.getInt buf)]
        {:solved true :cost cost :cycles cycles}))))

(defn read-solution-header
  [^ByteBuffer buf]
  (when (not= [7 0 0 0] (repeatedly 4 #(.get buf)))
    (throw (ex-info "bad header" {:buf buf})))
  (let [pzl-name (read-str buf)
        sol-name (read-str buf)
        info (read-solution-info buf)
        _ (expect 2 (.getInt buf) "solution header 1")
        more (when (:solved info)
               (let [area (.getInt buf)
                     _ (expect 3 (.getInt buf) "solution header 2")
                     unk (.getInt buf)]
                 {:area area
                  #_#_:objects n-objects
                  :unk unk
                  :objects (.getInt buf)}))]
    (merge
     {:puzzle pzl-name
      :solution sol-name}
     info
     more)))

(defn read-pos
  [^ByteBuffer buf]
  [(.get buf) (.get buf)])

(defn ->little-endian
  ^ByteBuffer [^ByteBuffer buf]
  (.order buf ByteOrder/LITTLE_ENDIAN))

(defn splat
  [m]
  (->> (seq m)
       (mapcat
         (fn [[k vs]]
           (if-not (coll? vs)
             [k vs]
             (apply concat
               (map-indexed
                 (fn [i v]
                   [(str k "-" i) v])
                 vs)))))
       (apply array-map)))

(defn strip-suffix
  [^String suf ^String s]
  (if-not (.endsWith s suf)
    s
    (.substring s 0 (- (.length s) (.length suf)))))

(defn unnest
  [m]
  (let [coll-keys (->> (seq m)
                       (filter #(coll? (val %)))
                       (map key))
        maxlength (apply max (map #(count (get m %)) coll-keys))]
    (list*
     (into {}
           (for [[k v] m]
             [k (if (coll? v) (first v) v)]))
     (->> (range 1 maxlength)
          (mapv
           (fn [i]
             (into {}
                   (for [k coll-keys]
                     [k (get (get m k) i)]))))))))

(defn looking-at?
  [^ByteBuffer b byte-vec]
  (loop [i 0]
    (cond
      (= i (count byte-vec)) true
      (>= i (.limit b)) false
      (= (.get b (+ i (.position b))) (nth byte-vec i)) (recur (inc i))
      :else false)))

(def ^:const step-decode-map
  (zipmap "RrEeGgPpAaCXO"
          [:rotate-cw :rotate-ccw
           :extend :retract
           :grab :drop
           :pivot-cw :pivot-ccw
           :track-forward :track-back
           :repeat :reset :nop]))

(defn read-program-step
  [^ByteBuffer buf]
  (let [chr (char (.get buf))
        stepnum (int (.get buf))]
    {:t stepnum
     :step (get step-decode-map chr (str chr)) 
     :bytes (vec (repeatedly 3 #(.get buf)))}))

(defn read-object
  [^ByteBuffer buf]
  (let [name (read-str buf) 
        _ (expect 1 (.get buf) (str "object header, after name=" (pr-str name)))
        positions? (vec (repeatedly 4 #(read-pos buf)))
        size (.getInt buf)
        id (.getInt buf)                ;not the ID
        index (.getInt buf)
        steps (.getInt buf)
        ;; extra-len is used for tracks
        extra-len (.getInt buf)
        object-header (cond-> {:name name :size size :index index :positions positions?}
                        (pos? id) (assoc :id id)
                        (pos? steps) (assoc :steps steps)
                        (pos? extra-len) (assoc :extra-len extra-len))]
    (cond-> object-header
      (pos? steps)
      (assoc :steps (vec (repeatedly steps #(read-program-step buf))))

      (and (= name "track") (pos? extra-len))
      (assoc :extra
             (let [extra (vec (for [i (range extra-len)]
                                [(.getInt buf) (.getInt buf)]))]
               (expect 0 (.getInt buf) (str "zero-padding after track, hdr=" (pr-str object-header)))
               extra)))))

(defn process-file
  [^File f]
  (try
    (let [buf (-> f read-to-buffer ->little-endian)
          header (read-solution-header buf)]
      (clojure.pprint/pprint header)
      (when (and (.hasRemaining buf) (:solved header))
        (clojure.pprint/print-table
         [:name :positions :size :index :steps :extra]
         (mapcat unnest
                 (for [i (range (:objects header))]
                   (read-object buf))))))
    (catch Exception e
      (throw (Exception. (str "reading " f) e)))))

(time
 (spit "/tmp/out"
       (with-out-str
        (doseq [f (file-seq (io/file "solutions"))
                :when (.endsWith (str f) ".solution")]
          (println (str f))
          (process-file f)))))

#_(process-file (io/file "solutions/alcohol-separation-1.solution"))


#_(let [f (io/file "solutions/alcohol-separation-1.solution")
      buf (-> f read-to-buffer ->little-endian)
      header (read-solution-header buf)]
  (clojure.pprint/pprint header)
  (while (.hasRemaining buf)
    (when (or (looking-at? buf (apply vector 7 (.getBytes "out-std")))
              (looking-at? buf (apply vector 5 (.getBytes "input")))
              (looking-at? buf (apply vector 5 (.getBytes "track")))
              (looking-at? buf (apply vector 5 (.getBytes "baron")))
              (looking-at? buf (apply vector 4 (.getBytes "arm1")))
              (looking-at? buf (apply vector 0x11 (.getBytes "glyph-duplication")))
              (looking-at? buf (apply vector 0x13 (.getBytes "glyph-calcification")))
              (looking-at? buf (apply vector 8 (.getBytes "unbonder")))
              (looking-at? buf (apply vector 6 (.getBytes "piston"))))
      (println)
      (print (format "%20s" (read-str buf))
             " " (.get buf)
             (format "%40s" (str (vec (repeatedly 4 #(read-pos buf)))))
             (str "len=" (.getInt buf))
             #_(format "%13s" (str (vec (repeatedly 4 #(.get buf)))))
             (format "%4s" (.getInt buf))
             (format "%4s" (.getInt buf))
             (format "%4s" (.getInt buf))
             (format "%4s" (.getInt buf))
                                        ;" " (.getInt buf)
             " "))
    (print (format "%02x " (.get buf)))))

