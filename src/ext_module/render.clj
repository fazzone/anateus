(ns ext-module.render
  (:require [clojure.java.io :as io]
            [ext-module.images :as images]
            [ext-module.model :as model])
  (:import [javax.imageio ImageIO]
           [java.awt.image BufferedImage]
           [java.awt Color BasicStroke]
           [java.awt.geom Point2D Path2D Path2D$Double]
           [java.io File]
           [java.awt Color Graphics2D]))


(defn hex-corner
  [x y size i]
  (let [angle (Math/toRadians (+ 30 (* 60 i)))]
    [(+ x (* size (Math/cos angle)))
     (+ y (* size (Math/sin angle)))]))

(defn hex-path
  [x y size]
  (let [path (Path2D$Double.)
        [ix iy] (hex-corner x y size 0)]
    (.moveTo path ix iy)
    (doseq [i (range 1 6)]
      (let [[x y] (hex-corner x y size i)]
        (.lineTo path x y)))
    (.closePath path)
    path))

(defn draw-image-centered
  [^Graphics2D g ^BufferedImage img x y]
  (let [w (.getWidth img)
        h (.getHeight img)]
    (.drawImage g img (- x (/ w 2)) (- y (/ h 2)) nil)))

(defn size->width
  [hexsize]
  (* 2 hexsize (/ (Math/sqrt 3) 2)))

(defn oddr-to-screen
  [hexsize x y]
  (let [vert (/ (* 3 hexsize) 2.0)
        width (size->width hexsize)]
    [(+ (* width x) (* (/ width 2) (bit-and y 1)))
     (* y vert)]))

(defn hex-at
  ([g hexsize x y]
   (hex-at g hexsize x y nil 0))
  ([g hexsize x y label]
   (hex-at g hexsize x y label 0))
  ([^Graphics2D g hexsize x y label lbloffset]
   (let [[xx yy] (oddr-to-screen hexsize x y)
         hexpath (hex-path xx yy hexsize)]
     (.draw g hexpath)
     (when label
       (let [tx (.getTransform g)]
         (.translate g xx yy)
         (.drawString g label -15 (+ 5 lbloffset))
         (.setTransform g tx))))))

(defn cube-to-oddr
  [x y z]
  [(+ x (/ (- z (bit-and z 1)) 2))
   z])

;;                 -4,+4    -3,+4    -2,+4    -1,+4    +0,+4
;;             -4,+3    -3,+3    -2,+3    -1,+3    +0,+3    +1,+3
;;         -4,+2    -3,+2    -2,+2    -1,+2    +0,+2    +1,+2    +2,+2 
;;     -4,+1    -3,+1    -2,+1    -1,+1    +0,+1    +1,+1    +2,+1    +3,+1 
;; -4,+0    -3,+0    -2,+0    -1,+0    +0,+0    +1,+0    +2,+0    +3,+0    +4,+0
;;     -3,-1    -2,-1    -1,-1    +0,-1    +1,-1    +2,-1    +3,-1    +4,-1
;;         -2,-2    -1,-2    +0,-2    +1,-2    +2,-2    +3,-2    +4,-2
;;             -1,-3    +0,-3    +1,-3    +2,-3    +3,-3    +4,-3
;;                 +0,-4    +1,-4    +2,-4    +3,-4    +4,-4


(defn axial-to-oddr
  [q r]
  (let [x (+ q r)
        z (- 0 r)]
    [(+ x (/ (- z (bit-and z 1)) 2))
     z]))

(defn axial-to-screen
  [hexsize i j]
  (let [[x y] (axial-to-oddr i j)]
    (oddr-to-screen hexsize x y)))

(defn draw-hex
  [^Graphics2D g hexsize i j label]
  (let [[x y] (axial-to-oddr i j)]
    (hex-at g hexsize x y label)))

(comment
  (let [[width-px height-px] [2560 1440]
        [center-x center-y] [(/ width-px 2) (/ height-px 2)]
        image (BufferedImage. width-px height-px BufferedImage/TYPE_INT_ARGB)
        ^Graphics2D g (.createGraphics image)
        hexsize 60]
    (.setColor g Color/white)
    (.fillRect g 0 0 width-px height-px)
  
    (.setColor g Color/black)
    (.translate g center-x center-y)
  
  
    (doseq [i (range -4 4)
            j (range -4 4)]
      (let [[x y] (axial-to-oddr i j)]
        (hex-at g hexsize x y (str i "," j))))
    #_(draw-image-centered g (get @images/type->image :gold) 0 0)
    (ImageIO/write image "png" (io/file "/tmp/image.png"))))
