(ns ext-module.hex)

;; canonical hex "radius"
(def ^:dynamic *size* 60)

(defrecord Point [x y])

(defn width
  []
  (* 2 *size* (/ (Math/sqrt 3) 2)))

(defn oddr-to-screen
  ([^Point p]
   (oddr-to-screen (.-x p) (.-y p)))
  ([x y]
   (let [vert (* 1.5 *size*)
         w (width)]
     (Point. (+ (* w x) (* (/ w 2.0) (bit-and y 1)))
             (* y vert)))))

(defn axial-to-oddr
  ([^Point p] (axial-to-oddr (.-x p) (.-y p)))
  ([q r]
   (let [x (+ q r)
         z (- 0 r)]
     (Point. (+ x (/ (- z (bit-and z 1)) 2))
             z))))

(defn axial-to-screen
  ([^Point ax]
   (oddr-to-screen (.-x ax) (.-y ax)))
  ([q r]
   (oddr-to-screen (axial-to-oddr q r))))


;;        [ x,  y,  z]
;;to  [-z, -x, -y]
;; function axial_to_cube(hex):
;;     var x = hex.q
;;     var z = hex.r
;;     var y = -x-z
;;     return Cube(x, y, z)
(defn rotate1
  [q r]
  (let [x (+ q r)
        z (- 0 r)
        y q]
    [(- 0 x) (- 0 z)]))

(rotate1 1 0)
(rotate1 -1 -1 )



(defn rotate
  ([steps ^Point ax]
   
   ))


(defn add
  [a b]
  (Point. (+ (:x a) (:x b))
          (+ (:y a) (:y b))))
(defn subtract
  [a b]
  (Point. (- (:x a) (:x b))
          (- (:y a) (:y b))))

(def ^:const hex-direction
  {:right (Point. 1 0)
   :left (Point. -1 0)
   :up-left (Point. -1 1)
   :up-right (Point. 0 1)
   :down-left (Point. 0 -1)
   :down-right (Point. 1 -1)})

(def ^:const angle->direction
  (mapv hex-direction [:right :down-right :down-left :left :up-left :up-right]))

(defn project
  [p hex-angle hexes]
  (let [d (angle->direction hex-angle)]
    (loop [h hexes
           p p]
      (if (zero? h)
        p
        (recur (dec h) (add p d))))))

(defn cw
  [i]
  (if (= i 6)
    0
    (inc i)))

(defn ccw
  [i]
  (if (= i 0)
    5
    (dec i)))

