(ns ext-module.read-solution
  (:require [clojure.java.io :as io])
  (:import [java.nio ByteBuffer ByteOrder]
           [java.nio.file Files]
           [java.io File]))

(defn read-to-buffer
  ^ByteBuffer [path]
  (-> path (io/file) (.toPath) (Files/readAllBytes) (ByteBuffer/wrap)))

(defn read-str
  [^ByteBuffer b]
  (let [len (.get b)]
    (apply str (repeatedly len #(char (.get b))))))

(defn expect
  ([exp val info]
   (when (not= val exp)
     (printf "warning - expected %s but got %s (%s) \n" exp val info))))

(defn read-solution-info
  [^ByteBuffer buf]
  (let [n (.getInt buf)]
    (if (zero? n)
      {:solved false}
      (let [unkA (.getInt buf)
            cycles (.getInt buf)
            unkB (.getInt buf)
            cost (.getInt buf)]
        {:solved true :cost cost :cycles cycles}))))

(defn read-solution-header
  [^ByteBuffer buf]
  (when (not= [7 0 0 0] (repeatedly 4 #(.get buf)))
    (throw (ex-info "bad header" {:buf buf})))
  (let [pzl-name (read-str buf)
        sol-name (read-str buf)
        info (read-solution-info buf)
        nobjects? (.getInt buf)
        more (if-not (:solved info)
               {:nobjects nobjects?}
               (let [area (.getInt buf)
                     _ (expect 3 (.getInt buf) "solved solution header unknown")
                     unk (.getInt buf)]
                 (expect 2 nobjects? "this should be 2 for solved files")
                 {:area area
                  :unk unk
                  :nobjects (.getInt buf)}))]
    (merge {:puzzle pzl-name :solution sol-name}
           info
           more)))

(defn read-pos
  [^ByteBuffer buf]
  [(.get buf) (.get buf)])

(defn read-pos-quad
  [^ByteBuffer buf]
  [(.getInt buf) (.getInt buf)])

(defn ->little-endian
  ^ByteBuffer [^ByteBuffer buf]
  (.order buf ByteOrder/LITTLE_ENDIAN))

#_(defn splat
  [m]
  (->> (seq m)
       (mapcat
         (fn [[k vs]]
           (if-not (coll? vs)
             [k vs]
             (apply concat
               (map-indexed
                 (fn [i v]
                   [(str k "-" i) v])
                 vs)))))
       (apply array-map)))

#_(defn strip-suffix
  [^String suf ^String s]
  (if-not (.endsWith s suf)
    s
    (.substring s 0 (- (.length s) (.length suf)))))

#_(defn unnest
  [m]
  (let [coll-keys (->> (seq m)
                       (filter #(coll? (val %)))
                       (map key))
        maxlength (apply max (map #(count (get m %)) coll-keys))]
    (list*
     (into {}
           (for [[k v] m]
             [k (if (coll? v) (first v) v)]))
     (->> (range 1 maxlength)
          (mapv
           (fn [i]
             (into {}
                   (for [k coll-keys]
                     [k (get (get m k) i)]))))))))

#_(defn looking-at?
  [^ByteBuffer b byte-vec]
  (loop [i 0]
    (cond
      (= i (count byte-vec)) true
      (>= i (.limit b)) false
      (= (.get b (+ i (.position b))) (nth byte-vec i)) (recur (inc i))
      :else false)))

(def ^:const step-decode-map
  (zipmap "RrEeGgPpAaCXO"
          [:rotate-cw :rotate-ccw
           :extend :retract
           :grab :drop
           :pivot-cw :pivot-ccw
           :track-forward :track-back
           :repeat :reset :nop]))

(defn read-program-step
  [^ByteBuffer buf]
  (let [chr (char (.get buf))
        stepnum (int (.get buf))]
    {:t stepnum
     :step (get step-decode-map chr (str chr)) 
     :bytes (vec (repeatedly 3 #(.get buf)))}))

(defn read-object
  [^ByteBuffer buf]
  (let [name (read-str buf) 
        _ (expect 1 (.get buf) (str "object header, after name=" (pr-str name)))
        position (read-pos-quad buf)
        size (.getInt buf)
        rotation (.getInt buf)
        index (.getInt buf)
        steps (.getInt buf)
        ;; extra-len is used for tracks
        extra-len (.getInt buf)
        object-header (cond-> {:name name :size size
                               :index index :position position
                               :rotation rotation}
                        (pos? steps) (assoc :steps steps)
                        (pos? extra-len) (assoc :extra-len extra-len))]
    (cond-> object-header
      (pos? steps)
      (assoc :steps (vec (repeatedly steps #(read-program-step buf))))

      (and (= name "track") (pos? extra-len))
      (assoc :extra
             (let [extra (vec (for [i (range extra-len)]
                                [(.getInt buf) (.getInt buf)]))]
               (expect 0 (.getInt buf) (str "zero-padding after track, hdr=" (pr-str object-header)))
               extra)))))

(defn process-file
  [^File f]
  (try
    (let [buf (-> f read-to-buffer ->little-endian)
          header (read-solution-header buf)]
      (cond-> header
        (.hasRemaining buf)
        (assoc :objects
               (vec (for [i (range (:nobjects header))]
                      (read-object buf))))))
    (catch Exception e
      (throw (Exception. (str "reading " f) e)))))

(def ^:const atom-decode-vec
  [nil :salt :air :earth :fire :water :quicksilver :gold :silver
   :copper :iron :tin :lead :vitae :mors :repeat :quintessence])

(def ^:const bond-decode-map
  {1 :normal
   2 :red
   4 :black
   8 :yellow
   0x0e :triplex})

(defn read-molecule
  [^ByteBuffer buf]
  (let [nprimes (.getInt buf)
        primes (vec
                (for [i (range nprimes)]
                  {:prime (nth atom-decode-vec (.get buf))
                   :position [(.get buf) (.get buf)]}))
        nbonds (.getInt buf)
        bonds (vec
               (for [i (range nbonds)]
                 {:bond-type (get bond-decode-map (.get buf))
                  :source (read-pos buf)
                  :dest (read-pos buf)}))]
    {:primes primes
     :bonds bonds}))

(defn process-puzzle
  [^File f]
  (let [buf (-> f read-to-buffer ->little-endian)
        _ (expect 2 (.getInt buf) "puzzle first bytes")
        name (read-str buf)
        steamid (.getLong buf)
        enabled (.getInt buf)
        _ (expect 0 (.getInt buf) "puzzle zero before defs")
        reagents (vec (repeatedly (.getInt buf) #(read-molecule buf)))
        products (vec (repeatedly (.getInt buf) #(read-molecule buf)))]
    (when (.hasRemaining buf)
      (expect 1 (.get buf) "puzzle end byte")
      (expect 0 (.getInt buf) "puzzle end zeroes"))
    {:name name
     :steamid steamid
     :enabled-mask enabled
     :reagents reagents
     :products products
     :position (.position buf)}))

(comment
  (->> (file-seq (io/file "/home/russell/.local/share/Opus Magnum/76561198048020892/"))
       (filter #(.endsWith (str %) ".puzzle"))
       #_(filter #(.contains (str %) "w1300"))
       (mapv (juxt str process-puzzle))))

