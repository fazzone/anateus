(ns ext-module.render.atoms
  (:require [ext-module.hex :as hex]
            [ext-module.svg :as s]))

;; ELEMENTS
(defn triangle-heavy-base
  [x y side]
  (let [c 0.93
        r (/ (Math/sqrt 3) 6)
        cx x
        cy (+ y (* r 2 side))
        lx (- x (/ side 2.0))
        ly (- y (* r side))
        rx (+ x (/ side 2.0))
        ry (- y (* r side))

        drx (- cx lx)
        dry (- cy ly)]
    (s/translate 0 (* (- y cy) r 0.5)
     [:g
      [:polygon
       {:fill "none"
        :points (s/points* cx cy lx ly rx ry)}]
      [:polygon
       {:points (s/points* (+ lx (* c side)) ly
                           rx ry
                           cx cy
                           (+ lx (* c drx)) (+ ly (* c dry)))}]
      #_[:ellipse {:cx x :cy y :rx 1 :ry 1}]])))

(defmulti draw-symbol (fn [atom-type x y size] atom-type))

(defmethod draw-symbol :water
  [_ x y side]
  (triangle-heavy-base x y side))

(defmethod draw-symbol :fire
  [_ x y side]
  [:g {:transform "scale(-1, -1)"} (draw-symbol :water x y side)])

(defmethod draw-symbol :earth
  [_ x y side]
  (let [dx (/ side 2.0)
        dy (/ side 8.0)]
    [:g
     (triangle-heavy-base x y side)
     [:polyline {:points (s/points* (- x dx) (+ y dy)
                                    (+ x dx) (+ y dy))}]]))
(defmethod draw-symbol :air
  [_ x y side]
  [:g {:transform "scale(1, -1)"} (draw-symbol :earth x y side)])

(defmethod draw-symbol :salt
  [_ x y size]
  (let [oh (* 0.6 size)
        ow (* 0.5 size)]
    [:g {:fill "none"}
     [:ellipse {:stroke-width 6 :cx x :cy y :rx ow :ry oh}]
     [:polyline {:points (s/points* (- x ow) y (+ x ow) y)}]]))

(defmethod draw-symbol :lead
  [_ x y size]
  [:g {:fill "none" :stroke "black" :stroke-width "4"
       :transform "scale(1.5) translate(-12.1,-18.2)"}
   [:path {:stroke-miterlimit 4
           :d "M 5.892849999999953,0.0 L 5.892849999999953,29.000000000000057"}]
   [:path {:d "M -0.8,4.75 L 13.0,4.75"}]
   [:path {:stroke-miterlimit 4
           :d "M 19.892849999999953,35.00000000000006 C 18.892849999999953,36.00000000000006 17.892849999999953,37.00000000000006 16.892849999999953,37.00000000000006 C 15.892849999999953,37.00000000000006 13.892849999999953,36.00000000000006 13.892849999999953,34.00000000000006 C 13.892849999999953,32.00000000000006 14.892849999999953,30.000000000000057 16.892849999999953,28.000000000000057 C 18.892849999999953,26.0 20.892849999999953,22.0 20.892849999999953,18.0 C 20.892849999999953,14.0 18.892849999999953,10.0 14.892849999999953,10.0 C 11.109659999999963,10.0 7.892849999999953,12.0 5.892849999999953,16.0"}]])

(def ^:const atom-size-ratio 0.7)

(defn atom-size
  []
  (* atom-size-ratio hex/*size*))

(defn atom-group
  ([t] (atom-group t 0 0))
  ([t x y]
   (let [s (atom-size)
         #_ #_r (* 0.8 hex/*size*)
         r (* 1.1 s)]
     [:g {:stroke "black" :stroke-width 3.3}
      [:ellipse {:cx x :cy y :rx s :ry s :fill "#bbb" :stroke-width 2}]
      (draw-symbol t x y r)])))


(defn bond-group*
  [sx sy tx ty]
  (let [bond-width (* 0.2 hex/*size*)
        bond-length (* 0.5 hex/*size*)
        sp (hex/axial-to-screen sx sy)
        tp (hex/axial-to-screen tx ty)
        ;; unit vector in the direction source -> target
        dx (/ (- (:x tp) (:x sp)) 2 hex/*size*)
        dy (/ (- (:y tp) (:y sp)) 2 hex/*size*)
        ;; midpoint between source/target hexes
        mx (+ (:x sp) (* dx hex/*size*))
        my (+ (:y sp) (* dy hex/*size*))
        ;; bond-length away from the midpoint towards the SOURCE
        smx (- mx (* dx bond-length))
        smy (- my (* dy bond-length))
        ;; bond-length away from the midpoint towards the TARGET
        tmx (+ mx (* dx bond-length))
        tmy (+ my (* dy bond-length))
        ;; perpendicular to dx/dy
        px (- 0 dy)
        py dx]
    [:polygon
     {:points
      (s/points*
       (- smx (* bond-width px)) (- smy (* bond-width py))
       (- tmx (* bond-width px)) (- tmy (* bond-width py))
        
       (+ tmx (* bond-width px)) (+ tmy (* bond-width py))
       (+ smx (* bond-width px)) (+ smy (* bond-width py)))}]))

(defn bond-group
  [sx sy tx ty]
  [:g {:fill "#444"
       :stroke "black"}
   (bond-group* sx sy tx ty)])
