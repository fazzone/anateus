(ns ext-module.render.parts
  (:require [ext-module.hex :as hex]
            [ext-module.svg :as s]
            [ext-module.render.atoms :as render-atoms]
            [ext-module.render.hex :as render-hex]))

(defn with-rotation
  [rot elem]
  (if rot
    (s/rotate (* 60 rot) elem)
    elem))

(defn arm-group
  [size]
  (let [dot-size (* 0.2 hex/*size*)
        circle-size (* 0.4 hex/*size*)
        arm-width (* 0.3 hex/*size*)
        gripper-radius (* (/ 2.0 3) hex/*size*)
        gripper-stroke (* 0.2 hex/*size*)]
    [:g {:stroke "black"}
     [:ellipse {:fill "#555" :cx 0 :cy 0 :rx circle-size :ry circle-size}]
     [:rect {:fill "#555"
             :x 0
             :y (- 0 (/ arm-width 2))
             :width (- (* size (hex/width)) gripper-radius)
             :height arm-width}]
     [:ellipse {:cx (* size (hex/width))
                :cy 0
                :rx gripper-radius
                :ry gripper-radius
                :fill "none"
                :stroke-width gripper-stroke}]
     [:ellipse {:cx (* size (hex/width))
                :cy 0
                :rx gripper-radius
                :ry gripper-radius
                :fill "none"
                :stroke "#555"
                :stroke-width (- gripper-stroke 2)}]

     [:ellipse {:fill "#aaa" :cx 0 :cy 0 :rx dot-size :ry dot-size}]]))


(defn double-bonder-group
  []
  (let [s (render-atoms/atom-size)
        bonder-circle [:ellipse {:cx 0 :cy 0 :rx s :ry s}]]
    [:g {:stroke "#444" :fill "#eee" :stroke-width 2}
     (render-atoms/bond-group* 0 0 1 0)
     bonder-circle
     (render-hex/at-hex 1 0 bonder-circle)]))
