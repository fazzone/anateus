(ns ext-module.render.hex
  (:require [ext-module.hex :as hex]
            [ext-module.svg :as s]))

(defn hex-group
  [x y]
  [:polygon
   {:points
    (s/points*
     #_(+ x (* hex/*size* (Math/cos (Math/toRadians 0))))
     #_(+ y (* hex/*size* (Math/sin (Math/toRadians 0))))

     (+ x (* hex/*size* (Math/cos (Math/toRadians 30))))
     (+ y (* hex/*size* (Math/sin (Math/toRadians 30))))

     (+ x (* hex/*size* (Math/cos (Math/toRadians 90))))
     (+ y (* hex/*size* (Math/sin (Math/toRadians 90))))

     (+ x (* hex/*size* (Math/cos (Math/toRadians 150))))
     (+ y (* hex/*size* (Math/sin (Math/toRadians 150))))

     (+ x (* hex/*size* (Math/cos (Math/toRadians 210))))
     (+ y (* hex/*size* (Math/sin (Math/toRadians 210))))

     (+ x (* hex/*size* (Math/cos (Math/toRadians 270))))
     (+ y (* hex/*size* (Math/sin (Math/toRadians 270))))
     
     (+ x (* hex/*size* (Math/cos (Math/toRadians 330))))
     (+ y (* hex/*size* (Math/sin (Math/toRadians 330)))))}])

(defn hex-board
  [xdim ydim]
  [:g {:fill "none" :stroke "#888" :stroke-width "1"}
   (for [i (range (- 0 xdim) xdim)
         j (range (- 0 ydim) ydim)
         :let [{:keys [x y]} (hex/axial-to-screen i j)]]
     [:g
      (hex-group x y)
      [:text {:fill "black" :font-size "12" :x x :y y} (str i "," j)]])])

(defn at-hex
  "translate `elem` so that 0,0 becomes the center of hex at axial coords x,y "
  [x y elem]
  (let [screen-point (hex/axial-to-screen x y)]
    (s/translate (:x screen-point) (:y screen-point) elem)))

